%% Simulation range
range_ofdm_power = 10.^(( -10:1:20 )/10);
range_gmsk_power = 10.^(( -20:1:10 )/10);

%Create vector of one zero power and one range
sim_n = length(range_ofdm_power) + length(range_gmsk_power);
input_ofdm_power = [range_gmsk_power*0 range_ofdm_power];
input_gmsk_power = [range_gmsk_power range_ofdm_power*0];

disp(['Simulate ' num2str(sim_n) ' times']);

%% Prepare the model
% Build the model
disp('Building model...');
model = 'parallel_ber_sim_model';
load_system(model);
Simulink.BlockDiagram.buildRapidAcceleratorTarget(model);
simin = Simulink.SimulationInput(model);   % Preinitialize input structures
simin = simin.setModelParameter('SimulationMode', 'rapid-accelerator');
simin = simin.setModelParameter('RapidAcceleratorUpToDateCheck', 'off');
simin = simin.setVariable('empty_tones', 0);

% Copy model and set specific values
disp('Configuring parameters...');
simin = repmat(simin, [sim_n 1]);
for idx = 1:sim_n
    simin(idx) = simin(idx).setVariable('ofdm_power', input_ofdm_power(idx));
    simin(idx) = simin(idx).setVariable('gmsk_power', input_gmsk_power(idx));
end

%% Simulate
disp('Simulate...');
simout = parsim(simin, 'ShowSimulationManager', 'off');

%% Store results
tones_n = length(simout(1).ofdm_errors);
output_ofdm_errors = reshape([simout.ofdm_errors], [tones_n sim_n]);
output_ofdm_errors = output_ofdm_errors(:, length(range_gmsk_power)+1:end);
output_gmsk_errors = [simout.gmsk_errors];
output_gmsk_errors = output_gmsk_errors(1:length(range_gmsk_power));
save(['results_zeros' char(datetime('now','Format','yyyyMMdd_HHmmss'))], 'range_ofdm_power', 'range_gmsk_power', 'output_ofdm_errors', 'output_gmsk_errors');



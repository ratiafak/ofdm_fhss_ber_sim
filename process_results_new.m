
%% Load
s64 = load('results_20200127_080514.mat');    %Load a set of  gmsk power / ofdm power / number of empy tones


% These data have error in GMSK error counter, it is not 2x as much, errors
% are relative to symbols
s128_em  = load('results_e0_20220210_130916.mat' );
s128_e0  = load('results_e0_20220210_141535.mat' );
s128_e1  = load('results_e1_20220210_152334.mat' );
s128_e3  = load('results_e3_20220211_101923.mat' );
s128_e5  = load('results_e5_20220211_112414.mat' );
s128_e7  = load('results_e7_20220211_122652.mat' );
s128_e9  = load('results_e9_20220211_133337.mat' );
s128_e11 = load('results_e11_20220211_143957.mat');

% These data have error in GMSK error counter, it is not 4x as much, errors
% are relative to symbols
s256_em  = load('results_e0_20220211_165300.mat' );
s256_e0  = load('results_e0_20220211_185156.mat' );
s256_e1  = load('results_e1_20220211_205107.mat' );
s256_e3  = load('results_e3_20220211_225001.mat' );
s256_e5  = load('results_e5_20220212_004849.mat' );
s256_e7  = load('results_e7_20220212_024736.mat' );
s256_e9  = load('results_e9_20220212_044638.mat' );
s256_e11 = load('results_e11_20220212_064543.mat');
s256_e13 = load('results_e13_20220212_084442.mat');
s256_e15 = load('results_e15_20220212_104337.mat');
s256_e17 = load('results_e17_20220212_124241.mat');
s256_e19 = load('results_e19_20220212_144155.mat');
s256_e21 = load('results_e21_20220212_164058.mat');

db_ofdm = log10(s64.range_ofdm_power)*10;
db_gmsk = log10(s64.range_gmsk_power)*10;
symbols = s64.symbols;
if (s64.symbols ~= s128_e0.symbols) || (s64.symbols ~= s256_e0.symbols)
    disp("Number of symbols doesn't fit");
end

%% OFDM 256 tones
f = figure(1);
pl_globallinewidth = 0.5;

tone0 = find(s256_e1.output_ofdm_errors(:, 1, 2) == 0);  %Find the tone where GMSK is centered
maxtone = 50;
ofdmpower = 7;

errors = (s256_em.output_ofdm_errors(tone0:tone0+maxtone, db_ofdm == ofdmpower) ...
    + s256_em.output_ofdm_errors(tone0:-1:tone0-maxtone, db_ofdm == ofdmpower))/(2 * 2 * symbols); %BER
semilogy(0:maxtone, errors);

% Plot improvements
set(gca, 'Linewidth', pl_globallinewidth);
grid('on');
xlabel('Tone Offset');
ylabel(['BER at ' num2str(ofdmpower) ' dB']);
xlim([-1 maxtone + 1]);

% Print
f.PaperUnits = 'centimeters';
f.PaperPosition = [0 0 16 11];
%print(f, '-depsc', 'fig_ofdm256_tone.eps');

%% Fixed OFDM power how hole size affects GMSK
f = figure(2);
pl_globallinewidth = 0.5;

semilogy(nan, nan, 'x', 'Color', 'none');   % Empty for legend title
hold('on');
line_colors = parula(3+1)*0.8;
ofdm_0_idx = find(db_ofdm == 0, 1);

% Plot 64 tone variant
% errors = s64.output_gmsk_errors(:, ofdm_0_idx, 1)/symbols; %BER
% semilogy(db_gmsk, errors, 'x-', 'Color', 'red', 'Linewidth', 1);
errors = s64.output_gmsk_errors(:, ofdm_0_idx, 2)/symbols; %BER
semilogy(db_gmsk, errors, 'x-', 'Color', line_colors(1, :), 'Linewidth', 1);
errors = s64.output_gmsk_errors(:, ofdm_0_idx, 3)/symbols; %BER
semilogy(db_gmsk, errors, 'x-', 'Color', line_colors(2, :), 'Linewidth', 1);
errors = s64.output_gmsk_errors(:, ofdm_0_idx, 4)/symbols; %BER
semilogy(db_gmsk, errors, 'x-', 'Color', line_colors(3, :), 'Linewidth', 1);

semilogy(nan, nan, 'x', 'Color', 'none');   % Empty for legend title
semilogy(nan, nan, 'x', 'Color', 'none');   % Empty for legend title

% Plot 128 tone variant
% errors = s128_e0.output_gmsk_errors(:)/(symbols*2); %BER
% semilogy(db_gmsk, errors, 'x', 'Color', 'red', 'Linewidth', 1);
errors = s128_e1.output_gmsk_errors(:)/symbols; %BER
semilogy(db_gmsk, errors, 'x--', 'Color', line_colors(1, :), 'Linewidth', 1);
errors = s128_e5.output_gmsk_errors(:)/symbols; %BER
semilogy(db_gmsk, errors, 'x--', 'Color', line_colors(2, :), 'Linewidth', 1);
errors = s128_e9.output_gmsk_errors(:)/symbols; %BER
semilogy(db_gmsk, errors, 'x--', 'Color', line_colors(3, :), 'Linewidth', 1);

semilogy(nan, nan, 'x', 'Color', 'none');   % Empty for legend title
semilogy(nan, nan, 'x', 'Color', 'none');   % Empty for legend title

% Plot 256 tone variant
% errors = s256_e0.output_gmsk_errors(:)/(symbols*4); %BER
% semilogy(db_gmsk, errors, 'x--', 'Color', 'red', 'Linewidth', 1);
errors = s256_e3.output_gmsk_errors(:)/symbols; %BER
semilogy(db_gmsk, errors, 'x:', 'Color', line_colors(1, :), 'Linewidth', 1);
errors = s256_e11.output_gmsk_errors(:)/symbols; %BER
semilogy(db_gmsk, errors, 'x:', 'Color', line_colors(2, :), 'Linewidth', 1);
errors = s256_e19.output_gmsk_errors(:)/symbols; %BER
semilogy(db_gmsk, errors, 'x:', 'Color', line_colors(3, :), 'Linewidth', 1);

hold('off');
% Plot improvements
legend({'n = 64', '4/256', '12/256', '20/256', ' ', ...
    'n = 128', '2/256', '10/256', '18/256', ' ', ...
    'n = 256', '3/256', '11/256', '21/256'}, 'Location', 'best');
set(gca, 'Linewidth', pl_globallinewidth);
grid('on');
xlabel('GMSK Signal Power [dB]');
ylabel('BER');

% Print
f.PaperUnits = 'centimeters';
f.PaperPosition = [0 0 16 11];
print(f, '-depsc', 'fig_gmsk_multif.eps');


%% Fixed GMSK power, how tone errors rise near GMSK
f = figure(3);
pl_globallinewidth = 0.5;

semilogy(nan, nan, 'x', 'Color', 'none');   % Empty for legend title
hold('on');
line_colors = parula(4+1)*0.8;
gmsk_0_idx = find(db_gmsk == 0, 1);
bits_ofdm = symbols * 2 * 2;    %Number of bits transmitted on two tones with the same distance from tone0

% Plot 64 tone variant
tone0 = find(s64.output_ofdm_errors(:, 2, 2, 2) == 0);  %Find the tone where GMSK is centered
errors = permute(sum(s64.output_ofdm_errors([tone0+1 tone0-1], gmsk_0_idx, :, 1), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x-', 'Color', line_colors(1, :), 'Linewidth', 1);
errors = permute(sum(s64.output_ofdm_errors([tone0+2 tone0-2], gmsk_0_idx, :, 1), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x-', 'Color', line_colors(2, :), 'Linewidth', 1);
errors = permute(sum(s64.output_ofdm_errors([tone0+3 tone0-3], gmsk_0_idx, :, 1), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x-', 'Color', line_colors(3, :), 'Linewidth', 1);
errors = permute(sum(s64.output_ofdm_errors([tone0+4 tone0-4], gmsk_0_idx, :, 1), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x-', 'Color', line_colors(4, :), 'Linewidth', 1);

semilogy(nan, nan, 'x', 'Color', 'none');   % Empty for legend title
semilogy(nan, nan, 'x', 'Color', 'none');   % Empty for legend title

% Plot 128 tone variant
tone0 = find(s128_e1.output_ofdm_errors(:, 1, 2) == 0);  %Find the tone where GMSK is centered
errors = permute(sum(s128_em.output_ofdm_errors([tone0+1 tone0-1], :), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x--', 'Color', line_colors(1, :), 'Linewidth', 1);
errors = permute(sum(s128_em.output_ofdm_errors([tone0+3 tone0-3], :), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x--', 'Color', line_colors(2, :), 'Linewidth', 1);
errors = permute(sum(s128_em.output_ofdm_errors([tone0+5 tone0-5], :), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x--', 'Color', line_colors(3, :), 'Linewidth', 1);
errors = permute(sum(s128_em.output_ofdm_errors([tone0+7 tone0-7], :), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x--', 'Color', line_colors(4, :), 'Linewidth', 1);

semilogy(nan, nan, 'x', 'Color', 'none');   % Empty for legend title
semilogy(nan, nan, 'x', 'Color', 'none');   % Empty for legend title

% Plot 256 tone variant
tone0 = find(s256_e1.output_ofdm_errors(:, 1, 2) == 0);  %Find the tone where GMSK is centered
errors = permute(sum(s256_em.output_ofdm_errors([tone0+2 tone0-2], :), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x:', 'Color', line_colors(1, :), 'Linewidth', 1);
errors = permute(sum(s256_em.output_ofdm_errors([tone0+6 tone0-6], :), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x:', 'Color', line_colors(2, :), 'Linewidth', 1);
errors = permute(sum(s256_em.output_ofdm_errors([tone0+10 tone0-10], :), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x:', 'Color', line_colors(3, :), 'Linewidth', 1);
errors = permute(sum(s256_em.output_ofdm_errors([tone0+14 tone0-14], :), 1), [3 2 1])/bits_ofdm; %BER
semilogy(db_ofdm, errors, 'x:', 'Color', line_colors(4, :), 'Linewidth', 1);

hold('off');
% Plot improvements, 4/256, 12/256, 20/256
legend({'n = 64', '4/256', '12/256', '20/256', '28/256', ' ', ...
    'n = 128', '2/256', '10/256', '18/256', '26/256', ' ',...
    'n = 256', '3/256', '11/256', '19/256', '27/256'}, 'Location', 'best');
set(gca, 'Linewidth', pl_globallinewidth);
grid('on');
xlabel('OFDM Signal Power [dB]');
ylabel('BER');
xlim([-5, 15]);

% Print
f.PaperUnits = 'centimeters';
f.PaperPosition = [0 0 16 11];
print(f, '-depsc', 'fig_ofdm_multif.eps');


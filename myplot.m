
f = figure(1);
pl_globallinewidth = 0.5;

% Plot GMSK OFDM and combination with noise
plot((-512:511)/1024, plot_fft(out.ofdmpre));
hold('on');
plot((-512:511)/1024, plot_fft(out.gmskpre));
plot((-512:511)/1024, plot_fft(out.post));
hold('off');

% Plot improvements
xticks(-0.5:0.25:0.5);
legend({'OFDM with hole' 'GMSK' 'OFDM+GMSK+AGWN'}, 'Location', 'best');
set(gca, 'Linewidth', pl_globallinewidth);
grid('on');

xlabel('f/f_s');
ylabel('Power [dB]');

% Print
f.PaperUnits = 'centimeters';
f.PaperPosition = [0 0 16 11];
print(f, '-depsc', 'fig_signal.eps');
